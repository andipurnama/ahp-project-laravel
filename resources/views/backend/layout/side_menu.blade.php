<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
            <div class="pull-left image">
            	<img src="{{asset('assets/img/c_avatar5.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    Andi Purnama
                </p>
            	<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            	<li>
                	<a href="{{route('/')}}">
                  		<i class="fa fa-dashboard"></i> <span>Dashboard</span>
                      	<span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('criteria')}}">
                        <i class="fa fa-user"></i> <span>Criteria</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('alternative')}}">
                        <i class="fa fa-user"></i> <span>Alternatif</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('criteria-analysis')}}">
                        <i class="fa fa-user"></i> <span>Analisa Kriteria</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('alternative-analysis')}}">
                        <i class="fa fa-user"></i> <span>Analisa Alternatif</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('criteria-result')}}">
                        <i class="fa fa-user"></i> <span>Hasil Kriteria</span>
                    </a>
                </li>
			</li>
        </ul>
    </section>
</aside>