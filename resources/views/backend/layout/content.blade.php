
<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        	<h1>
            	Dashboard
            	<small>Control panel</small>
          	</h1>
          	@yield('breadcrumb')
        </section>
        <!-- Main content -->
        <section class="content">
          
        	@if(session()->has('error'))
          		<div class="alert alert-error alert-dismissible alert-dashboard">
              		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              		{{session()->get('error')}}
          		</div>
          	@endif
          
          	@if(session()->has('success'))
          		<div class="alert alert-success alert-dismissible alert-dashboard">
              		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              		{{session()->get('success')}}
          		</div>
          	@endif
          	@yield('content')
        </section>
  	</div>