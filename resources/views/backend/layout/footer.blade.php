
  <footer class="main-footer" style="text-align: right;" >
        <strong>Copyright &copy; 2021 <a href="{{route('/')}}">Andi Purnama</a>
  </footer>
      <!-- jQuery 3 -->
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
      <!-- Bootstrap 3.3.7 -->
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
      <!-- Slimscroll -->
        <script src="{{asset('assets/js/jquery.slimscroll.min.js')}}"></script>
      <!-- AdminLTE App -->
        <script src="{{asset('assets/js/adminlte.min.js')}}"></script>
    @yield('footer')