@extends('backend.layout.master')

@section('header')
    <link href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
    </ol>
@endsection

@section('content')
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible alert-dashboard">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{session()->get('error')}}
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="box-title">Analisa Kriteria</h3>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::open(['url' => route('criteria-analysis.store'), 'method'=>'POST', 'class' => 'form', 'id' => 'form-criteria-analysis']) }}
                        <input type="hidden" name="id" id="criteria-analysis-id">
                        <div class="form-group">
                            <label>Nama Kriteria 1</label>
                            <select name="criteria1" class="form-control" id="criteria1">
                                <option value="" selected disabled>Pilih Kriteria 1</option>
                                @foreach($criterias as $value)
                                <option value="{{ $value->id }}">
                                    {{ $value->criteria_name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Perbandingan Kriteria</label>
                            <select name="saaty_scale" class="form-control" id="comparison-scale">
                                <option value="" selected disabled>Pilih Perbandingan</option>
                                @foreach($saatyScale as $value)
                                <option value="{{ $value->id }}">
                                    {{ $value->id }} - {{ $value->desc }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Kriteria 2</label>
                            <select name="criteria2" class="form-control" id="criteria2">
                                <option value="" selected disabled>Pilih Kriteria 2</option>
                                @foreach($criterias as $value)
                                <option value="{{ $value->id }}">
                                    {{ $value->criteria_name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="btn btn-warning" id="btn-reset" type="reset" name="" value="Reset">
                            <input class="btn btn-success" id="btn-submit" type="submit" name="simpan" value="Simpan">
                        </div>
                    {{ Form::close() }}

                    <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Nama Kriteria 1</th>
                                <th>Perbandingan</th>
                                <th>Nama Kriteria 2</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                                @foreach($criteriaAnalysis as $value)
                                    <tr>
                                        <td>{{ $count }}</td>
                                        <td>{{ $value->crit1->criteria_name }}</td>
                                        <td>{{ $value->saatyScale->desc }}</td>
                                        <td>{{ $value->crit2->criteria_name }}</td>
                                        <td>
                                            <div class="row">
                                                <!-- <button class="btn btn-primary btn-edit" value="{{ $value->id }}"><i class="fa fa-edit"></i></button> -->
                                                <button class="btn btn-danger btn-delete" value="{{ $value->id }}"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @php($count++)
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-destroy">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus Data</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-message">
                        Are you sure want to delete this data ?
                    </p>
                </div>
                <div class="modal-footer">
                    {{ Form::open(['route'=>'criteria-analysis.destroy', 'method'=>'GET', 'class' => 'modal-form-delete']) }}
                        <input type="hidden" name='id' id="id-destroy">
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('backend.criteria-analysis.script')
@endsection