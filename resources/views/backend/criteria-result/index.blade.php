@extends('backend.layout.master')

@section('header')
    <link href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
    </ol>
@endsection

@section('content')
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible alert-dashboard">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{session()->get('error')}}
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="box-title">Analisa Kriteria</h3>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::open(['url' => route('criteria-result.store'), 'method'=>'POST', 'class' => 'form', 'id' => 'form-criteria-result']) }}
                        <table class="table">
                            <tr>
                                <th></th>
                                @foreach($criterias as $crit)
                                    <th> {{ $crit->criteria_name }} </th>
                                @endforeach
                            </tr>

                            @foreach($criterias as $key => $crit)
                                <tr>
                                    <td> {{ $crit->criteria_name }} </td>
                                    @for($i=1; $i<=$count; $i++)
                                        @if($i == $count)
                                            <td> 1 </td>
                                        @else
                                            <td> <span id="temp-{{ $key . $i }}" ></span> 0 </td>
                                        @endif
                                    @endfor
                                    @for($i=$no; $i >= 1 ; $i--)
                                        <td>
                                        <select name="saaty_scale[]" class="form-control comparison-scale" data-key="comparison-scale">
                                            <option value="" selected disabled>Pilih Perbandingan</option>
                                            @foreach($saatyScale as $value)
                                            <option value="{{ $value->id }}">
                                                {{ $value->id }} - {{ $value->desc }}
                                            </option>
                                            @endforeach
                                        </select>
                                        </td>
                                    @endfor
                                </tr>
                                @php($no--)
                                @php($count++)
                            @endforeach
                        </table>
                        <input class="btn btn-success" id="btn-submit" type="submit" name="simpan" value="Simpan">
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-destroy">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus Data</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-message">
                        Are you sure want to delete this data ?
                    </p>
                </div>
                <div class="modal-footer">
                    {{ Form::open(['route'=>'criteria-analysis.destroy', 'method'=>'GET', 'class' => 'modal-form-delete']) }}
                        <input type="hidden" name='id' id="id-destroy">
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('backend.criteria-analysis.script')
@endsection