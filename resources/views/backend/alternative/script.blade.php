<script>
    $('.btn-edit').on('click', function(){
        $.ajax({
            url : "{{ route('alternative.edit') }}",
            type : 'GET',
            dataType : 'json',
            data : {
                id : $(this).val(),
            },
            success : function(data){
                const result = JSON.parse(data.data);
                $('#alternative-id').val(result.id);
                $('#alternative-name').val(result.name);
                $('#btn-submit').addClass('btn-primary');
                $('#btn-submit').removeClass('btn-success');
                $('#btn-submit').val('Update');
            },
            error : function (xhr, status){
                console.log(status);
            },
            complete : function(){
                alreadyloading = false;
            }
        });
    });

    $(document).on('click', '.btn-delete', function() {
        $('#id-destroy').val( $(this).val());
        $('#modal-destroy').modal('show');
    });
    
    $(document).on('click', '#btn-reset', function() {
        $('#alternative-id').val('');
        $('#btn-submit').val('Save');
        $('#btn-submit').removeClass('btn-info');
        $('#btn-submit').addClass('btn-success');
    });
</script>