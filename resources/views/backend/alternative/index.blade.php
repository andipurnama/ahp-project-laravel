@extends('backend.layout.master')

@section('header')
    <link href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
    </ol>
@endsection

@section('content')
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible alert-dashboard">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{session()->get('error')}}
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="box-title">Tabel Alternatif</h3>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row-md-3">
                <div class="col-sm-5">
                    {{ Form::open(['url' => route('alternative.store'), 'method'=>'POST', 'class' => 'form-horizontal', 'id' => 'form-alternative']) }}
                        <input type="hidden" name="id" id="alternative-id">
                        <div class="form-group">
                            <label>Nama Alternatif</label>
                            <input class="form-control" type="text" name="name" value="" id="alternative-name">
                        </div>
                        <div class="form-group">
                            <input class="btn btn-warning" id="btn-reset" type="reset" name="" value="Reset">
                            <input class="btn btn-success" id="btn-submit" type="submit" name="" value="Save">
                        </div>
                    {{ Form::close() }}
                </div>
                <div class="col-sm-7">
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>Nama Alternatif</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            @foreach($alternatives as $value)
                                <tr>
                                    <td> {{ $count }}</td>
                                    <td>{{ $value->alternative_name }}</td>
                                    <td>
                                        <div class="row">
                                            <button class="btn btn-primary btn-edit" value="{{ $value->id }}"><i class="fa fa-edit"></i></button>
                                            <button class="btn btn-danger btn-delete" value="{{ $value->id }}"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                @php($count++)
                            @endforeach 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-destroy">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus Data</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-message">
                        Are you sure want to delete this data ?
                    </p>
                </div>
                <div class="modal-footer">
                    {{ Form::open(['route'=>'alternative.destroy', 'method'=>'GET', 'class' => 'modal-form-delete']) }}
                        <input type="hidden" name='id' id="id-destroy">
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('backend.alternative.script')
@endsection