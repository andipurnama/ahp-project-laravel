<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CritComparison extends Model
{
    protected $table = 'crit_comparison';
    public $timestamps = true;
    
    protected $fillable = ['criteria1','saaty_scale_id','criteria2'];

    public function saatyScale()
    {
        return $this->belongsTo('app\Model\SaatyScale', 'saaty_scale_id');
    }

    public function crit1()
    {
        return $this->belongsTo('app\Model\Criteria', 'criteria1');
    }

    public function crit2()
    {
        return $this->belongsTo('app\Model\Criteria', 'criteria2');
    }
}
