<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Alternative extends Model
{
    protected $table = 'alternative';
    public $timestamps = true;
    
    protected $fillable = ['alternative_name'];
}
