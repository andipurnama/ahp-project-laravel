<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    protected $table = 'criterias';
    public $timestamps = true;
    
    protected $fillable = ['criteria_code','criteria_name'];
}
