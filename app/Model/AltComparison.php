<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AltComparison extends Model
{
    protected $table = 'alt_comparison';
    public $timestamps = true;
    
    protected $fillable = ['alternative1', 'saaty_scale_id', 'alternative2',];

    public function saatyScale()
    {
        return $this->belongsTo('app\Model\SaatyScale', 'saaty_scale_id');
    }

    public function alter1()
    {
        return $this->belongsTo('app\Model\Alternative', 'alternative1');
    }

    public function alter2()
    {
        return $this->belongsTo('app\Model\Alternative', 'alternative2');
    }
}
