<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SaatyScale extends Model
{
    protected $table = 'saaty_scale';
    public $timestamps = true;
    
    protected $fillable = ['desc'];
}
