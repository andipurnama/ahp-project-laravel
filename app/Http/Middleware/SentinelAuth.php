<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class SentinelAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            }
            if ($request->path() == 'login') {
                return $next($request);
            }
            return redirect()->route('login');
        } else {
            $role = user_info()->roles()->get()[0];
            $last_login = strtotime(Sentinel::getUser()->last_login);
            $max = config('session.lifetime') * 60; // min to hours conversion
            if (($max < (time() - $last_login))) {   
                Sentinel::logout();
                return redirect()->route('login');
            }
        }
        return $next($request);
    }
}
