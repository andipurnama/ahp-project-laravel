<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Criteria;

class CriteriaController extends Controller
{
    public function index()
    {
        $data['criterias'] = Criteria::all();
        $data['count'] = 1;
        return view('backend.criteria.index', $data);
    }

    public function store(Request $request)
    {
        if (isset($request->id)) {
            Criteria::find($request->id)->update([
                'criteria_code' => $request->code,
                'criteria_name' => $request->name,
            ]);    
        } else {
            Criteria::create([
                'criteria_code' => $request->code,
                'criteria_name' => $request->name,
            ]);
        } 
        return redirect()->to(route('criteria'));
    }

    public function edit(Request $request)
    {
        $meta = ['code' => 200, "error" => false, "message" => "Success"];
        $criteria = Criteria::find($request->id);
        $data = json_encode([
            'id' => $criteria->id,
            'code' => $criteria->criteria_code,  
            'name' => $criteria->criteria_name, 
        ]);
    
        return response()->json(['meta' => $meta, 'data' => $data], 200);
    }

    public function destroy(Request $request)
    {
        $criteria = Criteria::find($request->id)->delete();
        return redirect()->to(route('criteria'));
    }
}
