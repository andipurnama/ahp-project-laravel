<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Criteria;
use App\Model\SaatyScale;


class CriteriaResultController extends Controller
{
    public function index()
    {
        $data['criterias'] = Criteria::all();
        $data['no'] = count($data['criterias']) - 1;
        $data['count']= 1;
        $data['saatyScale'] = SaatyScale::all();
        return view('backend.criteria-result.index', $data);
    }

    public function store(Request $request) 
    {
        $modelcriteria = Criteria::all();
        $count = 1;
        $index1 = 0;
        $index2 = 0;
        $criteria = [];
        $no = count($modelcriteria);
        $start = 2;
        foreach($modelcriteria as $key=>$value) {

            for($i=2; $i <= $no ; $i++) {
                if($i>=$start){
                    $criteria[$value->criteria_code.$i]=$request->saaty_scale[$index2];
                    $index2++;
                }
            }
            $start++;
        }
        foreach ($modelcriteria as $key => $value) {
            foreach ($modelcriteria as $key2 => $value2) {
                if($value == $value2){
                    $criteria[$value->criteria_code.($key+1)]=1;
                }else{
                    if(!isset($criteria[$value2->criteria_code.($key2+1)])){
                        $criteria[$value2->criteria_code.($key+1)]=round(1/$request->saaty_scale[$index1],2);
                        $index1++;
                    }
                }
            }
        }
        dd($request->all(), $criteria);
    }
}
