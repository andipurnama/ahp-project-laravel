<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Alternative;

class AlternativeController extends Controller
{
    public function index()
    {
        $data['alternatives'] = Alternative::all();
        $data['count'] = 1;
        return view('backend.alternative.index', $data);
    }

    public function store(Request $request)
    {
        if (isset($request->id)) {
            Alternative::find($request->id)->update([
                'alternative_name' => $request->name
            ]);    
        } else {
            Alternative::create([
                'alternative_name' => $request->name
            ]);
        } 
        return redirect()->to(route('alternative'));
    }

    public function edit(Request $request)
    {
        $meta = ['code' => 200, "error" => false, "message" => "Success"];
        $alternative = Alternative::find($request->id);
        $data = json_encode([
            'id' => $alternative->id, 
            'name' => $alternative->alternative_name, 
        ]);
    
        return response()->json(['meta' => $meta, 'data' => $data], 200);
    }

    public function destroy(Request $request)
    {
        $alternative = Alternative::find($request->id)->delete();
        return redirect()->to(route('alternative'));
    }
}
