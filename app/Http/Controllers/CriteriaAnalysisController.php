<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\CritComparison;
use App\Model\Criteria;
use App\Model\SaatyScale;

class CriteriaAnalysisController extends Controller
{
    public function index()
    {
        $data['criteriaAnalysis'] = CritComparison::all();
        $data['criterias'] = Criteria::all();
        $data['saatyScale'] = SaatyScale::all();
        $data['count'] = 1;
        return view('backend.criteria-analysis.index', $data);
    }

    public function store(Request $request)
    {
        if (isset($request->id)) {
            CritComparison::find($request->id)->update([
                'criteria1' => $request->criteria1,
                'saaty_scale_id' => $request->saaty_scale,
                'criteria2' => $request->criteria2,
            ]);    
        } else {
            CritComparison::create([
                'criteria1' => $request->criteria1,
                'saaty_scale_id' => $request->saaty_scale,
                'criteria2' => $request->criteria2,
            ]);
        } 
        return redirect()->to(route('criteria-analysis'));
    }

    // public function edit(Request $request)
    // {
    //     $meta = ['code' => 200, "error" => false, "message" => "Success"];
    //     $crit_comparison = CritComparison::find($request->id);
    //     $data = json_encode([
    //         'id' => $crit_comparison->id, 
    //         'criteria1' => $crit_comparison->criteria1, 
    //         'saaty_scale' => $crit_comparison->saaty_scale_id, 
    //         'criteria2' => $crit_comparison->criteria2, 
    //     ]);
    
    //     return response()->json(['meta' => $meta, 'data' => $data], 200);
    // }

    public function destroy(Request $request)
    {
        $crit_comparison = CritComparison::find($request->id)->delete();
        return redirect()->to(route('criteria-analysis'));
    }
}
