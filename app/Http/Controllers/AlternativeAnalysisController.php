<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AltComparison;
use App\Model\Alternative;
use App\Model\SaatyScale;

class AlternativeAnalysisController extends Controller
{
    public function index()
    {
        $data['altAnalysis'] = AltComparison::all();
        $data['alternatives'] = Alternative::all();
        $data['saatyScale'] = SaatyScale::all();
        $data['count'] = 1;
        return view('backend.alternative-analysis.index', $data);
    }

    public function store(Request $request)
    {
        if (isset($request->id)) {
            AltComparison::find($request->id)->update([
                'alternative1' => $request->alternative1,
                'saaty_scale_id' => $request->saaty_scale,
                'alternative2' => $request->aternative2,
            ]);    
        } else {
            AltComparison::create([
                'alternative1' => $request->alternative1,
                'saaty_scale_id' => $request->saaty_scale,
                'alternative2' => $request->alternative2,
            ]);
        } 
        return redirect()->to(route('alternative-analysis'));
    }

    // public function edit(Request $request)
    // {
    //     $meta = ['code' => 200, "error" => false, "message" => "Success"];
    //     $crit_comparison = CritComparison::find($request->id);
    //     $data = json_encode([
    //         'id' => $crit_comparison->id, 
    //         'criteria1' => $crit_comparison->criteria1, 
    //         'saaty_scale' => $crit_comparison->saaty_scale_id, 
    //         'criteria2' => $crit_comparison->criteria2, 
    //     ]);
    
    //     return response()->json(['meta' => $meta, 'data' => $data], 200);
    // }

    public function destroy(Request $request)
    {
        $alt_comparison = AltComparison::find($request->id)->delete();
        return redirect()->to(route('alternative-analysis'));
    }
}
