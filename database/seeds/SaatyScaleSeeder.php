<?php

use Illuminate\Database\Seeder;

class SaatyScaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('saaty_scale')->truncate();
        
        DB::table('saaty_scale')->insert([
            'desc' => 'Sama penting dengan',
        ]);

        DB::table('saaty_scale')->insert([
            'desc' => 'Mendekati sedikit lebih penting dari',
        ]);

        DB::table('saaty_scale')->insert([
            'desc' => 'Sedikit lebih penting dari',
        ]);

        DB::table('saaty_scale')->insert([
            'desc' => 'Mendekati lebih penting dari',
        ]);

        DB::table('saaty_scale')->insert([
            'desc' => 'Lebih penting dari',
        ]);

        DB::table('saaty_scale')->insert([
            'desc' => 'Mendekati sangat penting dari',
        ]);

        DB::table('saaty_scale')->insert([
            'desc' => 'Sangat penting dari',
        ]);

        DB::table('saaty_scale')->insert([
            'desc' => 'Mendekati mutlak penting dari',
        ]);

        DB::table('saaty_scale')->insert([
            'desc' => 'Mutlak penting dari',
        ]);
    }
}
