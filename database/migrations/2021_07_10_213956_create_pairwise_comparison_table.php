<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairwiseComparisonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pairwise_comparison', function (Blueprint $table) {
            $table->increments('id');
            $table->float('pc1');
            $table->float('pc2');
            $table->float('pc3');
            $table->float('pc4');
            $table->float('pc5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pairwise_comparison');
    }
}
