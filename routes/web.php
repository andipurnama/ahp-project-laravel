<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => '/', 'uses' => 'Backend\MasterController@index']);

Route::get('/criteria', ['as' => 'criteria', 'uses' => 'CriteriaController@index']);
Route::post('/criteria/store', ['as' => 'criteria.store', 'uses' => 'CriteriaController@store']);
Route::get('/criteria/edit', ['as' => 'criteria.edit', 'uses' => 'CriteriaController@edit']);
Route::get('/criteria/destroy', ['as' => 'criteria.destroy', 'uses' => 'CriteriaController@destroy']);

Route::get('/alternative', ['as' => 'alternative', 'uses' => 'AlternativeController@index']);
Route::post('/alternative/store', ['as' => 'alternative.store', 'uses' => 'AlternativeController@store']);
Route::get('/alternative/edit', ['as' => 'alternative.edit', 'uses' => 'AlternativeController@edit']);
Route::get('/alternative/destroy', ['as' => 'alternative.destroy', 'uses' => 'AlternativeController@destroy']);

Route::get('/criteria-analysis', ['as' => 'criteria-analysis', 'uses' => 'CriteriaAnalysisController@index']);
Route::post('/criteria-analysis/store', ['as' => 'criteria-analysis.store', 'uses' => 'CriteriaAnalysisController@store']);
Route::get('/criteria-analysis/edit', ['as' => 'criteria-analysis.edit', 'uses' => 'CriteriaAnalysisController@edit']);
Route::get('/criteria-analysis/destroy', ['as' => 'criteria-analysis.destroy', 'uses' => 'CriteriaAnalysisController@destroy']);

Route::get('/alternative-analysis', ['as' => 'alternative-analysis', 'uses' => 'AlternativeAnalysisController@index']);
Route::post('/alternative-analysis/store', ['as' => 'alternative-analysis.store', 'uses' => 'AlternativeAnalysisController@store']);
Route::get('/alternative-analysis/destroy', ['as' => 'alternative-analysis.destroy', 'uses' => 'AlternativeAnalysisController@destroy']);

Route::get('/criteria-result', ['as' => 'criteria-result', 'uses' => 'CriteriaResultController@index']);
Route::post('/criteria-result/store', ['as' => 'criteria-result.store', 'uses' => 'CriteriaResultController@store']);
